#pragma once

#include <cstdlib>


int StandartValue;

struct MassivData {
	int CellFilled = 0;
	int SizeMultiply = 1;
	int CurrentCell = 0;
};

template<typename T>
class OurList
{
private:
	bool* IsCellUsed;
public:
	T* DMassive;
	MassivData MyData;
	OurList(int St_Value)
	{
		StandartValue = St_Value;
		DMassive = (T*)malloc(sizeof(T) * StandartValue);
		IsCellUsed = (bool*)malloc(sizeof(bool) * StandartValue);
		for (int i = 0; i < StandartValue; i++)
			IsCellUsed[i] = false;
	}
	OurList(const OurList<T>& donor) 
	{
		if (&donor != nullptr)
		{
		DMassive = (T*)malloc(sizeof(T) * StandartValue * (int(donor.MyData.SizeMultiply)));
		IsCellUsed = (bool*)malloc(sizeof(bool) * StandartValue * (int(donor.MyData.SizeMultiply)));
		for (int i = 0; i < StandartValue * (int(donor.MyData.SizeMultiply)); i++)
		{
			DMassive[i] = donor.DMassive[i];
			IsCellUsed[i] = donor.IsCellUsed[i];
		}
		MyData.CellFilled = donor.MyData.CellFilled;
		MyData.SizeMultiply = donor.MyData.SizeMultiply;
		MyData.CurrentCell = donor.MyData.CurrentCell;
		}
	}
	
	class IteratorS {
	public:
		int CurrentElementOfList;
		OurList* UrList;
		IteratorS(OurList<T> *TheirList)
		{
			UrList = TheirList;
			//begin();
		}
		IteratorS()
		{
			//begin();
		}
		T* GetLinkToElementMemory() { return (UrList->GetElementOfMassiveByPoint(CurrentElementOfList)); };
		int GetCurrentElementOfList() { return CurrentElementOfList; };
		int Increment()
		{
			CurrentElementOfList++;
			if ((CurrentElementOfList < UrList->GetNumberOfListSize() - 1) && (CurrentElementOfList > 0))
				return 1;
			else
				return 0;
		}
		int Decrement()
		{
			CurrentElementOfList--;
			if ((CurrentElementOfList < UrList->GetNumberOfListSize() - 1) && (CurrentElementOfList > 0))
				return 1;
			else
				return 0;
		}
		bool CompareEquality(int AnotherIteratorValue)
		{
			if (CurrentElementOfList == AnotherIteratorValue)
				return 1;
			else
				return 0;
		}
		bool CompareInequality(int AnotherIteratorValue)
		{
			if (CurrentElementOfList != AnotherIteratorValue)
				return 1;
			else
				return 0;
		}

	};
	friend class IteratorS;
	class ReverseIterator {
	public:
		int CurrentElementOfList;
		OurList* UrList;
		ReverseIterator(OurList<T> *TheirList)
		{
			UrList = TheirList;
			CurrentElementOfList = 0;
		}
		ReverseIterator()
		{
			CurrentElementOfList = 0;
		}
		T* GetLinkToElementMemory() { return (UrList->GetElementOfMassiveByPoint(CurrentElementOfList)); };
		int GetCurrentElementOfList() { return CurrentElementOfList; };
		int Increment()
		{
			CurrentElementOfList--;
			if ((CurrentElementOfList < UrList->GetNumberOfListSize() - 1) && (CurrentElementOfList > 0))
				return 1;
			else
				return 0;
		}
		int Decrement()
		{
			CurrentElementOfList++;
			if ((CurrentElementOfList < UrList->GetNumberOfListSize() - 1) && (CurrentElementOfList > 0))
				return 1;
			else
				return 0;
		}
		bool CompareEquality(int AnotherIteratorValue)
		{
			if (CurrentElementOfList == AnotherIteratorValue)
				return 1;
			else
				return 0;
		}
		bool CompareInequality(int AnotherIteratorValue)
		{
			if (CurrentElementOfList != AnotherIteratorValue)
				return 1;
			else
				return 0;
		}

	};
	friend class ReverseIterator;

	int OperationCounter(int WhatNeedToDo);
	void SpaceIncreace();
	void SpaceDecrease();
	void SpaceControl();
	int AddDataByPoint(T DataToAdd, int PositionInList);
	int AddDataToList(T DataToAdd);
	void ListEllementDelete(int DeletedPoint);
	int RemoveFromListByPoint(int point);
	int RemoveFromListByValue(T Value);
	int GetNumberOfListSize();
	int GetSpaceOfList();
	T GetValueOfListEllementByPoint(int point);
	void ClearList();
	int GetPointFromListByValue(T Value);
	bool IsItInList(T Value);
	int ChangeEllementByPoint(T Value, int point);
	void ShowAllEllements();
	void FillWithRandomValues(int NumberOfValues);
	//void IteratorsWork(int WorkVariant);
	//void IteratorCompare(int WorkVariant, int pointer);
	int CapacityOfList();
	T* GetListMassive();
	T* GetElementOfMassiveByPoint(int point);
	IteratorS StraitBegin()
	{
		IteratorS iter = IteratorS(this);
		MyData.CurrentCell = 0;
		iter.CurrentElementOfList = MyData.CurrentCell;
		return iter;
	}
	ReverseIterator ReverseBegin()
	{
		ReverseIterator iter = ReverseIterator(this);
		MyData.CurrentCell = MyData.CellFilled - 1;
		iter.CurrentElementOfList = MyData.CurrentCell;
		return iter;
	}
	IteratorS StraitEnd()
	{
		SpaceControl();
		IteratorS iter = IteratorS(this);
		MyData.CurrentCell = MyData.CellFilled;
		iter.CurrentElementOfList = MyData.CurrentCell;
		return iter;
	}
	ReverseIterator ReverseEnd()
	{
		ReverseIterator iter = ReverseIterator(this);
		MyData.CurrentCell = -1;
		iter.CurrentElementOfList = MyData.CurrentCell;
		return iter;
	}

};
template<typename T>
int OurList<T>::OperationCounter(int WhatNeedToDo)
{
	int CellUsed = 0;
	switch (WhatNeedToDo)
	{
	case 1:
		for (int i = 0; i < StandartValue * MyData.SizeMultiply; i++)
			if (IsCellUsed[i])
				CellUsed += 1;
		return (CellUsed);
		break;
	case 2:
		for (int i = 0; i < StandartValue * MyData.SizeMultiply; i++)
			IsCellUsed[i] = false;
		break;
	default:
		return 0;
		break;
	}
}
template<typename T>
void OurList<T>::SpaceIncreace()
{
	DMassive = (T*)realloc(DMassive, sizeof(T) * (StandartValue * (int(MyData.SizeMultiply) + 1)));
	IsCellUsed = (bool*)realloc(IsCellUsed, sizeof(bool) * (StandartValue * (int(MyData.SizeMultiply) + 1)));
	for (int i = 0; i < StandartValue * (int(MyData.SizeMultiply) + 1); i++)
		if (i < (StandartValue * (int(MyData.SizeMultiply))))
			IsCellUsed[i] = true;
		else IsCellUsed[i] = false;
	MyData.SizeMultiply += 1;
}
template<typename T>
void OurList<T>::SpaceDecrease()
{
	DMassive = (T*)realloc(DMassive, sizeof(T) * (StandartValue * (int(MyData.SizeMultiply) - 1)));
	IsCellUsed = (bool*)realloc(IsCellUsed, sizeof(bool) * (StandartValue * (int(MyData.SizeMultiply) - 1)));
	for (int i = 0; i < StandartValue * (int(MyData.SizeMultiply) - 1); i++)
		if (i < (StandartValue * (int(MyData.SizeMultiply) - 1)))
			IsCellUsed[i] = true;
		else 
			IsCellUsed[i] = false;
	MyData.SizeMultiply -= 1;
}
template<typename T>
void OurList<T>::SpaceControl()
{
	if (MyData.CellFilled >= StandartValue * MyData.SizeMultiply)
		SpaceIncreace();
	else if (((StandartValue * MyData.SizeMultiply) - (MyData.CellFilled - 1) > StandartValue) && (MyData.SizeMultiply != 1))
		SpaceDecrease();
}
template<typename T>
int OurList<T>::AddDataByPoint(T DataToAdd, int PositionInList)
{
	SpaceControl();
	if (PositionInList > MyData.CellFilled)
		return 0;
	else
	{
		DMassive[PositionInList] = DataToAdd;
		IsCellUsed[PositionInList] = true;
		MyData.CellFilled += 1;
		return 1;
	}
}
template<typename T>
int OurList<T>::AddDataToList(T DataToAdd)
{
	return(AddDataByPoint(DataToAdd, MyData.CellFilled));
}
template<typename T>
void OurList<T>::ListEllementDelete(int DeletedPoint)
{
	for (int i = DeletedPoint; i < MyData.CellFilled - 1; i++)
	{
		DMassive[i] = DMassive[i + 1];
		IsCellUsed[i] = true;
	}
	MyData.CellFilled = MyData.CellFilled - 1;
	SpaceControl();
}
template<typename T>
int OurList<T>::RemoveFromListByPoint(int point)
{
	if (point < MyData.CellFilled)
	{
		ListEllementDelete(point);
		return 1;
	}
	else
		return 0;
}
template<typename T>
int OurList<T>::RemoveFromListByValue(T Value)
{
	bool IsSomethingDeleted = false;
	for (int i = 0; i <= MyData.CellFilled - 1; i++)
	{
		IsCellUsed[i] = true;
		if (Value == DMassive[i])
		{
			RemoveFromListByPoint(i);
			i--;
			IsSomethingDeleted = true;
		}
	}
	if (IsSomethingDeleted)
		return 1;
	else
		return 0;
}
template<typename T>
int OurList<T>::GetNumberOfListSize()
{
	return MyData.CellFilled;
}
template<typename T>
int OurList<T>::GetSpaceOfList()
{
	if (MyData.CellFilled == 0)
		return 1;
	else
		return 0;
}
template<typename T>
T OurList<T>::GetValueOfListEllementByPoint(int point)
{
	if ((point < MyData.CellFilled)&& (point >=0))
	{
	IsCellUsed[point] = true;
	return DMassive[point];
	}
	return -1;
}
template<typename T>
void OurList<T>::ClearList()
{
	free (DMassive);
	free (IsCellUsed);
	MyData.SizeMultiply = 1;
	DMassive = (T*)malloc(sizeof(T) * StandartValue);
	IsCellUsed = (bool*)malloc(sizeof(bool) * StandartValue);
	MyData.CellFilled = 0;
}
template<typename T>
int OurList<T>::GetPointFromListByValue(T Value)
{
	bool IsNotSomeFind = false;
	int point;
	for (int i = 0; i <= MyData.CellFilled - 1; i++)
	{
		IsCellUsed[i] = true;
		if (Value == DMassive[i])
		{
			IsNotSomeFind = true;
			point = i;
			break;
		}
	}
	if (IsNotSomeFind)
		return point;
	else
		return -1;
}
template<typename T>
int OurList<T>::ChangeEllementByPoint(T Value, int point)
{
	if (point <= MyData.CellFilled)
	{
		IsCellUsed[point] = true;
		DMassive[point] = Value;
		return 1;
	}
	else
		return 0;
}
//template<typename T>
//OurList<T>::IteratorS OurList<T>::StraitBegin()
//{
//	IteratorS iter = IteratorS(*this);
//	MyData.CurrentCell = 0;
//	iter.CurrentElementOfList = MyData.CurrentCell;
//	return iter;
//}
//template<typename T>
//OurList<T>::ReverseIterator OurList<T>::ReverseBegin()
//{
//	ReverseIterator iter = ReverseIterator(*this);
//	MyData.CurrentCell = MyData.CellFilled - 1;
//	iter.CurrentElementOfList = MyData.CurrentCell;
//	return iter;
//}
//template<typename T>
//OurList<T>::IteratorS OurList<T>::StraitEnd()
//{
//	SpaceControl();
//	IteratorS iter = IteratorS(*this);
//	MyData.CurrentCell = MyData.CellFilled;
//	iter.CurrentElementOfList = MyData.CurrentCell;
//	return iter;
//}
//template<typename T>
//OurList<T>::ReverseIterator OurList<T>::ReverseEnd()
//{
//	ReverseIterator iter = ReverseIterator(*this);
//	MyData.CurrentCell = -1;
//	iter.CurrentElementOfList = MyData.CurrentCell;
//	return iter;
//}

//template<typename T>
//void OurList<T>::ShowAllEllements()
//{
//	printf("\n\n");
//	for (int i = 0; i <= MyData.CellFilled - 1; i++)
//	{
//		IsCellUsed[i] = true;
//		std::cout << "������� " << i << ": " << DMassive[i] << std::endl;
//	}
//	if (MyData.CellFilled == 0) printf("������ ����");
//	printf("\n\n");
//}

template <typename T>
T* OurList<T>::GetListMassive()
{
	return (DMassive);
}

template<typename T>
void OurList<T>::FillWithRandomValues(int NumberOfValues)
{
	int EarlyCell = MyData.CellFilled;
	for (int i = MyData.CellFilled; i < NumberOfValues + EarlyCell; i++)
	{
		SpaceControl();
		IsCellUsed[i] = true;
		DMassive[i] = rand() % 10000;
		MyData.CellFilled += 1;
	}
}

template<typename T>
T* OurList<T>::GetElementOfMassiveByPoint(int point)
{
	return (&DMassive[point]);
}

//template<typename T>
//void OurList<T>::IteratorsWork(int WorkVariant)
//{
//	switch (WorkVariant)
//	{
//	case 1:
//		std::cout << "\n\n������� ������: " << MyData.CurrentCell << "\n��������: " << DMassive[MyData.CurrentCell] << std::endl;
//		break;
//	case 2:
//		if (MyData.CurrentCell < MyData.CellFilled - 1)
//			MyData.CurrentCell += 1;
//		else
//			StraitBegin();
//		break;
//	case 3:
//		if (MyData.CurrentCell > 0)
//			MyData.CurrentCell -= 1;
//		else
//			ReverseBegin();
//		break;
//	default:
//		printf("\n\n��������� ������: ������� ���������� ���������� ������� �������\n\n");
//		break;
//	}
//}
//template<typename T>
//void OurList<T>::IteratorCompare(int WorkVariant, int pointer)
//{
//	switch (WorkVariant)
//	{
//	case 1:
//		if (MyData.CurrentCell != pointer)
//			printf("\n\n��������� ���������� �������\n\n");
//		else
//			printf("\n\n��������� ���������� �����\n\n");
//		break;
//	case 2:
//		if (MyData.CurrentCell == pointer)
//			printf("\n\n����������� ���������� �������\n\n");
//		else
//			printf("\n\n����������� ���������� �����\n\n");
//		break;
//	default:
//		printf("\n\n��������� ������: ������� ��������� ��������� ������� �������\n\n");
//		break;
//	}
//}
template<typename T>
int OurList<T>::CapacityOfList()
{
	return(StandartValue * MyData.SizeMultiply);
}

template<typename T>
bool OurList<T>::IsItInList(T value)
{
	bool IsNotSomeFind = false;
	int point;
	for (int i = 0; i <= MyData.CellFilled - 1; i++)
	{
		IsCellUsed[i] = true;
		if (value == DMassive[i])
		{
			IsNotSomeFind = true;
		}
	}
	if (IsNotSomeFind)
		return 1;
	else
		return 0;
}