﻿
#include <iostream>
#include <stdio.h>
#include <vector>



#include "OurType.h"


#pragma warning(disable:4996)

#define T int


void PrintMenu()
{
	printf("\n<--------------------->\n");
	printf("Меню программы: \n");
	printf("1)   Конструктор\n");
	printf("2)   Конструктор копирования\n");
	printf("3)   Деструктор\n");
	printf("4)   Опрос размера списка(кол-во элементов в списке)\n");
	printf("5)   Очистка списка\n");
	printf("6)   Проверка списка на пустоту\n");
	printf("7)   Опрос наличия заданного значения\n");
	printf("8)   Чтение значения с заданным номером в списке\n");
	printf("9)   Изменение значения с заданным номером в списке\n");
	printf("10)  Получение позиции в списке для заданного значения\n");
	printf("11)  Включение нового значения\n");
	printf("12)  Включение нового значения в позицию с заданным номером\n");
	printf("13)  Удаление заданного значения из списка\n");
	printf("14)  Удаление значения из позиции с заданным номером\n");
	printf("15)  Запрос прямого итератора begin()\n");
	printf("16)  Запрос обратного итератора rbegin()\n");
	printf("17)  Запрос «неустановленного» прямого итератора end()\n");
	printf("18)  Запрос «неустановленного» обратного итератора rend()\n");
	printf("19)  Операция доступа по чтению и записи к текущему значению *\n");
	printf("20)  Операция инкремента для перехода к следующему (к предыдущему для обратного итератора) значению в списке++\n");
	printf("21)  Операция декремента для перехода к предыдущему (к следующему для обратного итератора) значению в списке--\n");
	printf("22)  Проверка равенства однотипных итераторов ==\n");
	printf("23)  Проверка неравенства однотипных итераторов !=\n");
	printf("24)  Запрос числа элементов списка, просмотренных предыдущей операцией\n");
	printf("25)  Вывод на экран последовательности значений данных из списка\n");
	printf("26)  Заполнение случайными значениями\n");
	printf("27)  Опрос емкости списка списка(кол-во выделенных ячеек массива)\n");
	printf("28)  Выход\n");
	printf("<--------------------->\n");
	printf("<Авторы: Саланда А, Коньшин Д.>\n");
}

int MenuUserInput()
{
	int UserModeNumber;
	printf("\nВведите вариант работы: ");
	scanf("%d", &UserModeNumber);
	return UserModeNumber;
}

int MenuSelector()
{
	//system("cls");
	PrintMenu();
	return MenuUserInput();
}

int main()
{
	setlocale(LC_ALL, "Russian");

	int VariationOfWork = 0;
	bool IsItWorks = true, IsItInList;
	OurList<int> *UList = NULL, *UDList = NULL;
	int point, UsedCell, Standart_Value_Get, ReturnedValue, CellFilled, WhichIteratorIsActive, VariantOfCase;
	printf("Введите базовый размер массива: ");
	scanf("%d", &Standart_Value_Get);
	UList = new OurList<int>(Standart_Value_Get);
	OurList<int>::IteratorS IterS = UList->StraitBegin();
	OurList<int>::ReverseIterator IterR = UList->ReverseBegin();

	T Value;
	T* DMassivePointer = NULL;
	T* ElementPointer = NULL;

	 
	while (IsItWorks)
	{
		VariationOfWork = MenuSelector();
		switch (VariationOfWork)
		{
		case 0:
			printf("Произошла ошибка");
			break;
		case 1:
			printf("Введите базовый размер массива: ");
			scanf("%d", &Standart_Value_Get);
			UList = new OurList<int>(Standart_Value_Get);
			break;
		case 2:
			UDList = new OurList<int>(*UList);
			break;
		case 3:
			delete UList;
			UList = NULL;
			break;
		case 4:
			ReturnedValue = UList->GetNumberOfListSize();
			//printf("\n\nВ списке %d элементов\n\n", ReturnedValue);
			std::cout << ReturnedValue;
			break;
		case 5:
			UList->ClearList();
			break;
		case 6:
			std::cout << UList->GetSpaceOfList();
			//std::cout << ReturnedValue;
			/*switch (ReturnedValue)
			{
			case 0:
				printf("\n\nСписок пуст\n\n");
				std::cout << ReturnedValue;
				break;
			case 1:
				printf("\n\nСписок не пуст: Доступное количество элементов до увеличения: %d\n\n", ReturnedValue);
				break;
			default:
				break;
			}*/
			break;
		case 7:
			printf("Введите значение: ");
			std::cin >> Value;
			ReturnedValue = UList->IsItInList(Value);
			//if (ReturnedValue != -1)
			//	//printf("\n\nЯчейка, в которой встречается значение: %d\n\n", ReturnedValue);
			//else
			//	printf("\n\nЭлемент не найден\n\n");
			std::cout << ReturnedValue;
			break;

		case 8:
			printf("Введите номер ячейки: ");
			scanf("%d", &point);
			Value = UList->GetValueOfListEllementByPoint(point);
			std::cout << "\n\nЭлемент " << point << ": " << Value << "\n\n";
			break;
		case 9:
			printf("Введите значение и номер ячейки: ");
			std::cin >> Value;
			scanf("%d", &point);
			ReturnedValue = UList->ChangeEllementByPoint(Value, point);
			/*switch (ReturnedValue)
			{
			case -1:
				printf("\n\nОшибка: Данная ячейка не находиться в списке\n\n");
				break;
			default:
				break;
			}*/
			std::cout << ReturnedValue;
			break;
		case 10:
			printf("Введите значение: ");
			std::cin >> Value;
			ReturnedValue = UList->GetPointFromListByValue(Value);
			std::cout << "\n\n" << "Значение " << Value << " найдено в ячейке: " << ReturnedValue << "\n\n" << std::endl;
			break;
		case 11:
			printf("Введите значение: ");
			std::cin >> Value;
			ReturnedValue = UList->AddDataToList(Value);
			/*switch (ReturnedValue)
			{
			case -1:
				printf("\n\nОшибка: Выбранная ячейка добавления не находиться в списке\n\n");
				break;
			default:
				break;
			}*/
			std::cout << ReturnedValue;
			break;
		case 12:
			printf("Введите значение и номер ячейки: ");
			std::cin >> Value;
			scanf("%d", &point);
			ReturnedValue = UList->AddDataByPoint(Value, point);
			/*switch (ReturnedValue)
			{
			case -1:
				printf("\n\nОшибка: Выбранная ячейка добавления не находиться в списке\n\n");
				break;
			default:
				break;
			}*/
			std::cout << ReturnedValue;
			break;
		case 13:
			printf("Введите значение: ");
			std::cin >> Value;
			ReturnedValue = UList->RemoveFromListByValue(Value);
			/*switch (ReturnedValue)
			{
			case -1:
				printf("\n\nОшибка: Данная ячейка не находиться в списке\n\n");
				break;
			default:
				break;
			}*/
			std::cout << ReturnedValue;
			break;
		case 14:
			printf("Введите номер ячейки: ");
			scanf("%d", &point);
			ReturnedValue = UList->RemoveFromListByPoint(point);
			/*switch (ReturnedValue)
			{
			case -1:
				printf("\n\nОшибка: Данная ячейка не находиться в списке\n\n");
				break;
			default:
				break;
			}*/
			std::cout << ReturnedValue;
			break;
		case 15:
			IterS = UList->StraitBegin();
			WhichIteratorIsActive = 1;
			IsItInList = true;
			break;
		case 16:
			IterR = UList->ReverseBegin();
			WhichIteratorIsActive = 2;
			IsItInList = true;
			break;
		case 17:
			IterS = UList->StraitEnd();
			WhichIteratorIsActive = 1;
			IsItInList = true;
			break;
		case 18:
			IterR = UList->ReverseEnd();
			printf("\nНевозможно в массиве(Итератор установлен на -1 ячейку массива)\n");
			WhichIteratorIsActive = 2;
			IsItInList = false;
			break;
		case 19:
			if (IsItInList)
			{
				if (WhichIteratorIsActive == 1)
					ElementPointer = IterS.GetLinkToElementMemory();
				else if (WhichIteratorIsActive == 2)
					ElementPointer = IterR.GetLinkToElementMemory();
				else
				{
					printf("\n\n0\n\n");
					break;
				}
				printf("\n\nЗначение в ячейке: %d\n Вы хотите его изменить? (1 = да, 0 = нет)\n\n", *ElementPointer);
				scanf("%d", &VariantOfCase);
				if (VariantOfCase)
				{
					printf("\n\nВведите значение: ");
					std::cin >> Value;
					*ElementPointer = Value;
				}
			}
			else
				printf("\n\n-1 (вне списка)\n\n");
			break;
		case 20:
			if (WhichIteratorIsActive == 1)
				ReturnedValue =  IterS.Increment();
			else if (WhichIteratorIsActive == 2)
				ReturnedValue = IterR.Increment();
			else
				printf("\n\n-1\n\n");
			break;
			if (ReturnedValue == 0)
				IsItInList = false;
			else
				IsItInList = true;
		case 21:
			//UList->IteratorsWork(3);
			if (WhichIteratorIsActive == 1)
				ReturnedValue = IterS.Decrement();
			else if (WhichIteratorIsActive == 2)
				ReturnedValue = IterR.Decrement();
			else
				printf("\n\n-1\n\n");
			if (ReturnedValue == 0)
				IsItInList = false;
			else
				IsItInList = true;
			break;
		case 22:
			printf("Введите ячейку указания итератора для сравнения с текущим итератором: ");
			scanf("%d", &point);
			//UList->IteratorsWork(3);
			if (WhichIteratorIsActive == 1)
				ReturnedValue = IterS.CompareEquality(point);
			else if (WhichIteratorIsActive == 2)
				ReturnedValue = IterR.CompareEquality(point);
			else
				printf("\n\n-1\n\n");
			
			/*if (ReturnedValue)
				printf("\n\nИтераторы равны\n\n");
			else
				printf("\n\nИтераторы не равны\n\n");*/
			std::cout << ReturnedValue;
			//UList->IteratorCompare(1, point);
			break;
		case 23:
			printf("Введите ячейку укзания итератора для сравнения с текущим итератором: ");
			scanf("%d", &point);
			if (WhichIteratorIsActive == 1)
				ReturnedValue = IterS.CompareInequality(point);
			else if (WhichIteratorIsActive == 2)
				ReturnedValue = IterR.CompareInequality(point);
			else
				printf("\n\n-1\n\n");
			/*if (ReturnedValue)
				printf("\n\nИтераторы не равны\n\n");
			else
				printf("\n\nИтераторы равны\n\n");*/
			std::cout << ReturnedValue;
			//UList->IteratorCompare(2, point);
			break;
		case 24:
			printf("\n\nИспользованно ячеек в программе: %d\n\n", UsedCell);;
			break;
		case 25:
			//UList->ShowAllEllements();
			CellFilled = UList->GetNumberOfListSize();
			DMassivePointer = UList->GetListMassive(); printf("\n\n");
				for (int i = 0; i <= CellFilled - 1; i++)
				{
					std::cout << "Элемент " << i << ": " << DMassivePointer[i] << std::endl;
				}
				if (CellFilled == 0) printf("Список пуст");
				printf("\n\n");
			break;
		case 26:
			printf("Введите колличество ячеек для рандомного заполнения: ");
			scanf("%d", &point);
			UList->FillWithRandomValues(point);
			break;
		case 27:
			ReturnedValue = UList->CapacityOfList();
			printf("\nТекущая емкость списка: %d\n", ReturnedValue);
			break;
		case 28:
			IsItWorks = false;
			break;
		default:
			printf("Произошла ошибка");
			break;
		}
		if (!(IsItWorks)) break;
		if (UList != NULL)
		{
		UsedCell = UList->OperationCounter(1);
		UList->OperationCounter(2);
		}
		printf("\n");
		system("pause");
	}


}

